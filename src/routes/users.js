'use strict';

const { Router } = require('express');

module.export = (userService) => {
  const router = Router();

  router.use((req, res, next) => {
    // TODO: Auth logic
    next();
  });

  // TODO: add validation
  router.get('/:id', userService.createUser);
  router.put('/:id', userService.updateUser);
  router.delete('/:id', userService.deleteUser);
  router.get('/', userService.getAllUsers);
  router.post('/', userService.createUser);
}
