'use strict';

const { applySpec, pipe, pathOr } = require('ramda');

module.exports = applySpec({
  http: {
    port: pipe(pathOr('8000', ['HTTP_PORT']), Number),
  }
});
