'use strict';

const { Observable } = require('rxjs/Observable')
const fs = require('fs');

// implement object that reads&writes the json file and protects it from
// being written to by multiple requests

class DbPersistence {
  constructor(filePath) {
    this.filePath = filePath;
  }

  get() {

  }

  update() {

  }
}

module.exports = filePath => new DbPersistence(filePath);
