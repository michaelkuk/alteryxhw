'use strict';

const { Reader: reader } = require('monet');
const { unary, pathEq, equals, assoc } = require('ramda');
const uuid = require('uuid');

const addUserId = user => assoc('id', uuid.v4(), user)

const getUser = id => reader(
  ({ db }) => db.users.getById(id)
    .first()
);

const getAllUsers = () => reader(
  ({ db }) => db.users.getAll()
);

const userExists = user => reader(
  ({ db }) => db.users.getAll()
    .filter(unary(pathEq(['email'], user.email)))
    .first()
    .mapTo(true)
    .catch(() => Observable.of(false))
);

const createUser = user => reader(
  ({ db }) => userExists(user)
    .run({ db })
    .filter(unary(equals(false)))
    .first()
    .mapTo(addUserId(user)) // TODO: handle password hashing
    .mergeMap(db.users.upsert)
);

const updateUser = user => reader(
  ({ db }) => getUser(user.id)
    .run({ db })
    .mapTo(user) // TODO: handle password hashing
    .mergeMap(db.users.upsert)
);

const deleteUser = id => reader(
  ({ db }) => getUser(id)
    .run({ db })
    .mergeMap(() => db.users.remove(id))
);

module.exports = {
  getUser,
  getAllUsers,
  userExists,
  createUser,
  updateUser,
  deleteUser,
};
