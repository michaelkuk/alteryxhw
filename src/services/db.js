'use strict';

const { Observable } = require('rxjs/Rx');
const { Reader: reader } = require('monet');
const { unary, defaultTo, path, assocPath, dissocPath } = require('ramda');

const USERS_PROP = 'users';

const getUserById = id => reader(
  ({ persistence }) => persistence
    .get()
    .map(unary(path([USERS_PROP, id])))
);

const getAllUsers = () => reader(
  ({ persistence }) => persistence
    .get()
    .map(unary(pipe(path([USERS_PROP]), defaultTo({}), values)))
    .mergeAll()
);

const upsertUser = user => reader(
  ({ persistence }) => persistence
    .get()
    .map(unary(assocPath([USERS_PROP, user.id], user)))
    .mergeMap(unary(persistence.update))
    .mapTo(user)
);

const removeUser = id => reader(
  ({ persistence }) => persistence
    .get()
    .map(unary(dissocPath([USERS_PROP, id])))
    .mergeMap(unary(persistence.update))
    .mapTo(true)
);
