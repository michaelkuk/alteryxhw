'use strict';

const { Observable } = require('rxjs/Rx');
const jwt = require('jsonwebtoken');
const uuid = require('uuid');

const signToken = Observable.bindNodeCallback(jwt.sign)

const issueToken = userId => reader(
  ({ config }) => Observable.of({
    userId,
    jwtid: uuid.v4()
  })
    .mergeMap(token => signToken(token, config.auth.secret))
);
