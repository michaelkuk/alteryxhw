'use strict';

class AuthState {
  constructor() {
    this.invalidTokens = new Set();
  }

  isTokenInvalidated(tokenId) {
    return this.invalidTokens.has(tokenId);
  }
}

module.exports = () => new AuthState();
